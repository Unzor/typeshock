const fs = require('fs');
const { spawn } = require("child_process");

function fmt(str) {
	if (str.split('.').length >= 2) {
		return str.split('.').slice(0, -1).join('.').split('/').pop();
	} else {
		return str;
	}
}

function compile() {
	var file = process.argv[3];
	var command = `"${process.env.typeshock_home||"C:\\Program Files\\typeshock"+"\\Compiler"}\\tsc" --emit=jit -nogc -dump-object-file -object-filename=${fmt(file)}.o ${file} && "${process.env.typeshock_home||"C:\\Program Files\\typeshock"+"\\Compiler"}\\lld" -flavor link ${fmt(file)}.o /libpath:"${process.env.typeshock_home||"C:\\Program Files\\typeshock"+"\\Compiler\\"}Dependencies\\MSVC" /libpath:"${process.env.typeshock_home||"C:\\Program Files\\typeshock"+"\\Compiler\\"}Dependencies\\SDK\\um" /libpath:"${process.env.typeshock_home||"C:\\Program Files\\typeshock"+"\\Compiler\\"}Dependencies\\SDK\\ucrt" /defaultlib:libcmt.lib libvcruntime.lib`;
	console.log(command);
	spawn(command, {shell: true, stdio: "inherit"});
}

function run() {
	var file = 'src/main.ts';
	var command = `"${process.env.typeshock_home||"C:\\Program Files\\typeshock"+"\\Compiler"}\\tsc" --emit=jit -nogc -dump-object-file -object-filename=build/${fmt(file)}.o ${file} && "${process.env.typeshock_home||"C:\\Program Files\\typeshock"+"\\Compiler"}\\lld" -flavor link build/${fmt(file)}.o /libpath:"${process.env.typeshock_home||"C:\\Program Files\\typeshock"+"\\Compiler\\"}Dependencies\\MSVC" /libpath:"${process.env.typeshock_home||"C:\\Program Files\\typeshock"+"\\Compiler\\"}Dependencies\\SDK\\um" /libpath:"${process.env.typeshock_home||"C:\\Program Files\\typeshock"+"\\Compiler\\"}Dependencies\\SDK\\ucrt" /defaultlib:libcmt.lib libvcruntime.lib && mv ${fmt(file)} build && "build/${fmt(file)}.exe"`;
	spawn(command, {shell: true, stdio: "inherit"});
}

function init() {
	fs.mkdirSync(process.argv[3]);
	fs.mkdirSync(process.argv[3] + '/src')
	fs.mkdirSync(process.argv[3] + '/build')
	fs.writeFileSync(process.argv[3] + '/src/main.ts', `function main() {
	print('Hello, World!');
}`)
};

function help() {
	console.log(`TypeShock v1.0.0
Commands:
- compile: compiles a .ts file into an executable
- run: runs a project (needs to be ran in main directory of project)
- init: creates a new project
- help: shows this help prompt`)
}
let f = {
		init,
		run,
		compile,
		help
}
f[process.argv[2]]();
